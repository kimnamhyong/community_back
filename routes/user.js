const express = require("express");
const multer = require('multer');
const {User} = require("../models");
const path = require("path");
const bcrypt = require('bcrypt');
const {localPassport} = require("../util/login_util");
const router = express.Router();
//업로드를 할 때 필요로 함
const upload = multer({
    storage: multer.diskStorage({
        destination(req, file, cb) {
            console.log(req);
            cb(null, 'uploads/');
        },
        filename(req, file, cb) {
            const ext = path.extname(file.originalname);
            cb(null, path.basename(file.originalname, ext) + Date.now() + ext);
        },
    }),
    limits: { fileSize: 5 * 1024 * 1024 },
});

//아이디 중복체크하기
router.get("/id_check", async (req,res,next) => {
   try{
       const row = await User.findOne({
           where:{user_id:req.query.user_id}
       }) ;
       //아이디가 있을 경우 false
       if(row){
           return res.json({is_check:false});
        //없으면 true
       }else{
           return res.json({is_check:true});
       }
   } catch (e){
       console.log(e);
   }
});
//파일업로드
router.post('/upload', upload.single('image'), (req, res) => {
    return res.status(200).send(req.file.filename);
    //res.json({ url: `/img/${req.file.filename}` });
});
//회원가입
router.post("/register", async(req,res,next) => {
   try{
       console.log(req.body);


       const hashPassword =await bcrypt.hash(req.body.user_password,12);
       if(req.body.mode==='') {
           const row = await User.findOne({
               where:{user_id:req.body.user_id}
           });
           if(row){
               return res.status(403).json({error:'중복된 아이디가 있습니다'})
           }
           await User.create({
               user_id: req.body.user_id,
               user_password: hashPassword,
               user_name: req.body.user_name,
               user_hp: req.body.user_hp,
               user_email: req.body.user_email,
               user_profile: req.body.user_profile,
               user_level: 2,
           });
           return res.status(200).json({data:'회원가입이 완료되었습니다'});
       }else{
           let field ={
               user_name: req.body.user_name,
               user_hp: req.body.user_hp,
               user_email: req.body.user_email,
               user_profile: req.body.user_profile,
           }
           if(req.body.user_password!==''){
               field.user_password=hashPassword;
           }

           const result=await User.update(field,{
               where:{user_id:req.body.user_id}
           });
           console.log(result);
           const row =await User.findOne({
               where:{user_id:req.body.user_id}
           });
           return res.status(200).json(row);
       }


   } catch(err){
       console.log(err);
   }
});

//회원로그인
router.post("/login", async (req,res,next) => {
   try{
        if(req.body.type === "local"){
           await localPassport(req,res,next);
        }else{
            console.log("자동로그인");
        }
   }catch (e) {
       console.log(e);
   }
});

module.exports = router; // 라우터 내보내기