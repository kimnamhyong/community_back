const express = require("express");
const {User,Board} = require("../models");
const router = express.Router();

router.post("/write", async(req,res,next) => {
    try{
       if(req.body.mode==='update'){
           await Board.update({
               user_name: req.body.user_name,
               subject: req.body.subject,
               content: req.body.content
           },{
               where:{id:req.body.id}
           });
       } else {
           await Board.create({
               user_id: req.body.user_id,
               user_name: req.body.user_name,
               subject: req.body.subject,
               content: req.body.content
           });
       }
       return res.status(200).json({data : '게시판 등록 완료'});
    }catch(err){
        console.log(err);
    }
});
//게시판 목록
router.get("/list", async (req,res,next) => {
   try{
       const limit = 5;
       const page = req.query.page || 1;
       const fromRecord = (page-1) * limit;//limit 첫 번째 수
       const row = await Board.findAll({
           limit:[fromRecord,limit],
           order:[['id','desc']]
       });
       return res.status(200).json(row);
       
   } catch (err){

   }
});
//게시판 보기
router.get("/view", async (req,res,next) => {
    try{
        const row = await Board.findOne({
            where:{id:req.query.id}
        });
        return res.status(200).json(row);

    } catch (err){

    }
});
//게시판 삭제
router.delete("/remove/:id", async (req,res,next) => {
    try {
        await Board.destroy({
            where:{id:req.params.id}
        });
        return res.status(200).json({is_success:true});
    }catch{

    }
});

module.exports = router;