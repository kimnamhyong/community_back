const express = require("express");
const {User, BoardComment, GalleryComment} = require("../models");
const router = express.Router();

router.post("/:type/update", async (req, res, next) => {
    try {
        const type = req.params.type;
        const tableObject = type === "board" ? BoardComment : GalleryComment;
        if(req.body.mode==='update'){
            await tableObject.update({
               content:req.body.content
            },{
                where:{
                    id:req.body.comment_id
                }
            });
            return res.status(200).json({data: '댓글 수정 완료'});
        }else {

            await tableObject.create({
                user_id: req.body.user_id,
                user_name: req.body.user_name,
                content: req.body.content,
                board_id: req.body.board_id
            });
            return res.status(200).json({data: '댓글 등록 완료'});
        }
        
    } catch (err) {
        console.log(err);
    }
});
router.delete("/:type/remove/:id", async (req, res, next) => {
    try{
        const type = req.params.type;
        const tableObject = type === "board" ? BoardComment : GalleryComment;
        await tableObject.destroy({
            where:{
                id:req.params.id
            }
        });
        return res.status(200).json({is_success:true});
    }catch(e){

    }

});
router.get("/:type/list", async (req, res, next) => {
    try {
        const type = req.params.type;
        const tableObject = type === "board" ? BoardComment : GalleryComment;
        //페이지 관련
        const cntRow = await tableObject.findAndCountAll({
            where: {
                board_id: req.query.board_id
            }
        });
        const totalCount = cntRow.count;//광고 총갯수
        const page = req.query.page || 1;
        const limit = 5;
        const pageSize = 10;//보여주는 페이지 수
        const fromRecord = (page - 1) * limit;//limit 첫 번째 수
        const totalPage = Math.ceil(totalCount / limit);//총 페이지 수
        const startPage = (Math.ceil(page / pageSize) - 1) * pageSize + 1;//페이지 그룹에 첫번째 페이지
        let lastPage = startPage + pageSize + 1;//마지막 페이지
        lastPage = totalPage < lastPage ? totalPage : lastPage;
        const fromRecordPage = [];//한 그룹에 보여줄 페이지
        for (let i = startPage - 1; i < lastPage; i++) {
            fromRecordPage.push(i);
        }
        //프론트엔드에 보낼 페이징 데이터
        const pages = {
            pageSize: pageSize,
            fromRecord: fromRecord,
            totalPage: totalPage,
            startPage: startPage,
            lastPage: lastPage,
            totalCount:totalCount,
            limit:limit,
            fromRecordPage: fromRecordPage,
            page: parseInt(page)
        };


        const row = await tableObject.findAll({
            where: {
                board_id: req.query.board_id
            },
            limit: [fromRecord, limit],
            order: [['id', 'desc']]
        });

        let datas = JSON.stringify(row);
        datas=JSON.parse((datas));
        for(let i=0;i<datas.length;i++){
            const row2=await User.findOne({
                attributes:['user_profile'],
                where:{
                    user_id:datas[i].user_id
                }
            });

            datas[i].profile=row2.user_profile;
        }
        console.log(datas);
        return res.status(200).json({row:datas,pages:pages});
    } catch (error) {
        console.error(error);
    }
});

module.exports = router;