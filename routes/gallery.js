const express = require("express");
const {User,Gallery, Board} = require("../models");
const multer = require("multer");
const path = require("path");
const router = express.Router();

//업로드를 할 때 필요로 함
const upload = multer({
    storage: multer.diskStorage({
        destination(req, file, cb) {
            console.log(req);
            cb(null, 'uploads/');
        },
        filename(req, file, cb) {
            const ext = path.extname(file.originalname);
            cb(null, path.basename(file.originalname, ext) + Date.now() + ext);
        },
    }),
    limits: { fileSize: 5 * 1024 * 1024 },
});
router.post("/write", async(req,res,next) => {
    try{
        console.log(req.body);
        if(req.body.mode==='update'){
            await Gallery.update({
                user_name: req.body.user_name,
                subject: req.body.subject,
                content: req.body.content,
                image:req.body.image
            },{
                where:{id:req.body.id}
            });
        } else {
            await Gallery.create({
                user_id: req.body.user_id,
                user_name: req.body.user_name,
                subject: req.body.subject,
                content: req.body.content,
                image:req.body.image
            });
        }
        return res.status(200).json({data : '게시판 등록 완료'});
    }catch(err){
        console.log(err);
    }
});
//게시판 목록
router.get("/list", async (req,res,next) => {
    try{
        const limit = 9;
        const page = req.query.page || 1;
        const fromRecord = (page-1) * limit;//limit 첫 번째 수
        const row = await Gallery.findAll({
            limit:[fromRecord,limit],
            order:[['id','desc']]
        });
        return res.status(200).json(row);

    } catch (err){

    }
});
//게시판 보기
router.get("/view", async (req,res,next) => {
    try{
        const row = await Gallery.findOne({
            where:{id:req.query.id}
        });
        return res.status(200).json(row);

    } catch (err){

    }
});
//게시판 삭제
router.delete("/remove/:id", async (req,res,next) => {
    try {
        await Gallery.destroy({
            where:{id:req.params.id}
        });
        return res.status(200).json({is_success:true});
    }catch(error){
        console.log(error);
    }
});

//파일업로드
router.post('/upload', upload.single('image'), (req, res) => {
    return res.status(200).send(req.file.filename);
    //res.json({ url: `/img/${req.file.filename}` });
});

module.exports = router;