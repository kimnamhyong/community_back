const Sequelize = require("sequelize");
const User = require("./user");
const Board = require("./board");
const Gallery = require("./gallery");
const BoardComment = require("./board_comment");
const GalleryComment = require("./gallery_comment");
const env = process.env.NODE_ENV || "development";
const config = require("../config/config")[env]; //db정보가 있는 곳
const db = {}; //db 테이블을 객체로 받는 곳

const sequelize = new Sequelize( //db화
  config.database,
  config.username,
  config.password,
  config
);

db.sequelize = sequelize;
db.User = User; //db 모델명 설정
db.Board = Board;
db.Gallery = Gallery;
db.BoardComment = BoardComment;
db.GalleryComment = GalleryComment;



User.init(sequelize);
Board.init(sequelize);
Gallery.init(sequelize);
BoardComment.init(sequelize);
GalleryComment.init(sequelize);

User.associate(db);
Board.associate(db);
Gallery.associate(db);
BoardComment.associate(db);
GalleryComment.associate(db);



module.exports = db; //db 모듈을 내보내기
