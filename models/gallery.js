const Sequelize = require("sequelize");

module.exports = class Gallery extends Sequelize.Model {
    static init(sequelize) {
        return super.init(
            {

                user_name: {
                    type: Sequelize.STRING(255),
                    allowNull: true,
                    defaultValue: '',
                    comment: '작성자이름'
                },
                subject: {
                    type: Sequelize.STRING(255),
                    allowNull: true,
                    defaultValue: '',
                    comment: '제목'
                },
                content: {
                    type: Sequelize.TEXT('long'),
                    allowNull: true,
                    comment: '내용'
                },
                image: {
                    type: Sequelize.STRING(255),
                    allowNull: true,
                    comment: '이미지'
                },

            },
            {
                sequelize,
                timestamps: true,
                underscored: false,
                modelName: "Gallery",
                tableName: "gallery",
                paranoid: true,
                charset: "utf8",
                collate: "utf8_general_ci",
            }
        );
    }

    static associate(db) {
        db.Gallery.belongsTo(db.User,{ foreignKey: 'user_id', sourceKey:'user_id'});
        db.Gallery.hasMany(db.GalleryComment,{ foreignKey: 'board_id', sourceKey:'id'});
    }
};
