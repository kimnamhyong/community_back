const Sequelize = require("sequelize");

module.exports = class GalleryComment extends Sequelize.Model {
    static init(sequelize) {
        return super.init(
            {
                user_id:{
                    type: Sequelize.STRING(255),
                    allowNull: true,
                    defaultValue: '',
                    comment: '작성자아이디'
                },
                user_name: {
                    type: Sequelize.STRING(255),
                    allowNull: true,
                    defaultValue: '',
                    comment: '작성자이름'
                },

                content: {
                    type: Sequelize.TEXT('long'),
                    allowNull: true,
                    comment: '내용'
                },


            },
            {
                sequelize,
                timestamps: true,
                underscored: false,
                modelName: "GalleryComment",
                tableName: "gallery_comment",
                paranoid: true,
                charset: "utf8",
                collate: "utf8_general_ci",
            }
        );
    }

    static associate(db) {
        db.GalleryComment.belongsTo(db.Gallery,{ foreignKey: 'board_id', sourceKey:'id'});

    }
};
