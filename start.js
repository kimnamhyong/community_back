/* 윈도우 pm2 사용할 때 쓰임 */
const path = require("path");
const exec = require('child_process').exec;
const client = exec('npm run start', {
    windowsHide: true,
    cwd: path.join(__dirname, './'),
});

client.stdout.pipe(process.stdout);
client.stderr.pipe(process.stderr);
/* 여기까지 */